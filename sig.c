#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define SIG_THRESH	3.0

#define MAX_LENGTH	500
#define MAX_RECORD	10000		/* Maximum number of sequences for now */

#define SIG_NCOMPU	1.e-100

int nRecords = 0;

typedef struct s_record {
	char seqStr[24];
	int  seqID;
	int	 fReads;
	int	 rReads;
	int  nReads;
	char header[MAX_LENGTH];
	char sequence[MAX_LENGTH];
	
} S_RECORD;

S_RECORD records[MAX_RECORD];

char wt_sequence[MAX_LENGTH];


#define ERROR_PER_BASE	0.0005		/* Roche 454 substitution rate */
static int seqLen;					/* Sequence length			   */

double kf_betai(double a, double b, double x);
double compute_signficance (int mm, int NN)
{
	double betai;
	double k, n, p;
	double lam, fac;
	double Nmax;
	
	lam  = ERROR_PER_BASE * seqLen; /* expected number of errors per read										*/
	fac  = lam*exp(-1.*lam);		/* probability of reads with 1-error from target sequence					*/
	Nmax = NN/exp(-1.*lam);			/* NN reflects sequences left post noise, correct to estimate max counts	*/
	
	k = 1.*mm;						/* Counts of observed haplotype to test for significance					*/
	n = 1.*Nmax*fac;				/* Expected number of 1-aways from parent with estimate max counts Nmax		*/
	
 /* Correct n to the upper tail of its statistical range using +2sqrt(n) counting stats - as conservative		*/
	n += 2*sqrt(n);
	
	if (k >= n || ((n-k) < 1.0)) return (SIG_NCOMPU);
	
 /* Probability of matching at same site is p = 1/seqLen */
	p = 1./seqLen;
	betai = kf_betai(k, n-k-1, p);	/* Use incomplete beta to compute binomial tail */
	return (betai);
}

int main (int argc, char **argv)
{
	char *ptr;
	FILE *fp=NULL;
	FILE *fo=NULL;
	FILE *fs=NULL;
	FILE *ff=NULL;
	char buf[1024];
	char idStr[64];
	
	char outFileName[1280];
	char sigFileName[1280];
	char inFileName[1280];
	char inFileRoot[1280];
	char wtFileName[1280];
	
	int ndiff=0;
	int wdiff=0;
	int min_diffr;
	int i, j, k, len;
	int max_reads;
	int max_index;
	int seq1_reads, seq2_reads;
	char seq1[MAX_LENGTH];
	char seq2[MAX_LENGTH];
	
	double sig, prb;
	int tot_reads;
	
	/* Parse commandline arguments */
	if (argc != 2) {
		printf("\n\n");
		printf("*****************************************          \n\n");
		printf("Usage sig inFileName Amp1                          \n\n");
		printf("inFileName:  Locus    File name must include '.fna'\n\n");
		printf("*****************************************          \n\n");
		exit(1);
		
	} else {	/* Parse arguments */
		len = strlen (argv[1]);
		if (!strstr(argv[1], ".fna")) {
			printf("*****************************************  \n\n");
			printf("Error, inFileName does not contain '.fna' suffix\n\n");
			printf("*****************************************  \n\n");
			exit(1);
		}
		strcpy(inFileName, argv[1]);
		
		/* Check valid file */
		if ((fp = fopen (inFileName, "r")) == NULL) {
			printf("***************************************** \n\n");
			printf("Error, Unable to open input file: %s\n\n", inFileName);
			printf("***************************************** \n\n");
			exit(1);
		}
		strcpy(inFileRoot, inFileName);
		ptr = strstr(inFileRoot, ".fna");
		*ptr=0;	

	 /* Check valid locus in file name */
		if (strstr(argv[1],        "_Amp1_")) {
			printf("Amp1 Not implemented yet, exiting...\n");
			exit(1);
		} else if (strstr(argv[1], "Amp2")) {
			strcpy(wtFileName, "Amp2_WT.fna");
		} else if (strstr(argv[1], "Amp3")) {
			strcpy(wtFileName, "Amp3_WT.fna");
		} else if (strstr(argv[1], "Amp4")) {
			strcpy(wtFileName, "Amp4_WT.fna");
		} else {
			printf("***************************************** \n\n");
			printf("Error, invalid final name, no locus %s\n\n", argv[1]);
			printf("***************************************** \n\n");
			exit(1);
		}
		
		/* Check valid file */
		if ((ff = fopen (wtFileName, "r")) == NULL) {
			printf("***************************************** \n\n");
			printf("Error, Unable to open input file: %s\n\n", wtFileName);
			printf("***************************************** \n\n");
			exit(1);
		}
		
	 /* Now read in the WT reference sequence */
		fgets(buf, 1024, ff);
		fgets(buf, 1024, ff);
		sscanf(buf, "%s", wt_sequence);
		fclose(ff);
	}

 /* Output output file 1 */
	sprintf(outFileName, "%s.sig.all.txt", inFileRoot);
	if ((fo = fopen (outFileName, "w")) == NULL) {
		printf("***************************************** \n\n");
		printf("Error: unable to open output file: %s\n", outFileName);
		printf("***************************************** \n\n");
		exit(1);
	}
 /* Output output file 2 */
	sprintf(outFileName, "%s.sig.txt", inFileRoot);
	if ((fs = fopen (outFileName, "w")) == NULL) {
		printf("***************************************** \n\n");
		printf("Error: unable to open output file: %s\n", outFileName);
		printf("***************************************** \n\n");
		exit(1);
	}
	
 /* Load records from sequence file */
	while (fgets(buf, 1024, fp)) {
		sscanf(buf, "%s", idStr);
		strcpy(records[nRecords].seqStr, idStr);
		
	 /* Parse */
		ptr = strchr(idStr,   '_'); *ptr=' ';
		ptr = strchr(idStr,   '_'); *ptr=' ';
		ptr = strchr(idStr,   '_'); *ptr=' ';
		
	 /* Store */
		sscanf(idStr, " %s %d %d %d", buf, &(records[nRecords].seqID), &(records[nRecords].fReads), &(records[nRecords].rReads));
		records[nRecords].nReads = records[nRecords].fReads + records[nRecords].rReads;
		strcpy(records[nRecords].header, buf);
		
	 /* Get sequence */
		fgets(buf, 1024, fp);
		sscanf(buf, "%s", records[nRecords].sequence);
		nRecords++;
		
		if (nRecords == MAX_RECORD) {
			printf("*****************************************  \n\n");
			printf("Error, Too many sequences in input file: %s\n\n", inFileName);
			printf("*****************************************  \n\n");
			exit(1);
		}
	}
	fclose(fp);
	
 /* Now loop over each read and find maximum read count for 1-away sequences to compute significance */
	seqLen = strlen(records[0].sequence);
	
	for (i=0; i<nRecords; i++) {
		max_reads =  0;
		max_index = -1;
		tot_reads =  0;
		min_diffr = seqLen;
		
		seq1_reads = records[i].nReads;
		strcpy(seq1,records[i].sequence);
		
		for (j=0; j< nRecords; j++) {
			seq2_reads = records[j].nReads;
			strcpy(seq2,records[j].sequence);

		 /* Measure genetic distance */
			ndiff=0;
			for (k=0; k<seqLen; k++) if (seq1[k] != seq2[k]) ndiff++;
			
		 /* Check if seq_i is a one-away from seq_j */
			if (ndiff == 1) {	

			 /* If seq2_reads > seq1_reads - no need to test relative signficance */
				if (seq2_reads > seq1_reads) {
					tot_reads += seq2_reads;
					if (seq2_reads > max_reads) {
						max_index=j;
						max_reads=seq2_reads;
					}
				}
			}
			
		 /* Find minimum difference, but ignore self difference */
			if ((ndiff < min_diffr) && (i != j)) min_diffr = ndiff;	
		}

	 /* Measure genetic distance from WT sequence */
		wdiff=0;
		for (k=0; k<seqLen; k++) if (seq1[k] != wt_sequence[k]) wdiff++;
		
	 /* Check whether there were any 1-aways */
		if (max_index >= 0) {
			
		 /* Compute significance value for 1-away sequence with maximum counts */
		 // prb =  compute_signficance (seq1_reads, records[max_index].nReads);
			
			prb =  compute_signficance (seq1_reads, tot_reads);	// Use total reads for 1-aways > seq_reads
			sig = (prb) ? -log10(prb) : -log10(SIG_NCOMPU);
			fprintf(fo, "%s, %d, %d, %g, %d\n",records[i].seqStr, seq1_reads, min_diffr, sig, wdiff); 
			fprintf(fo, "%s\n",records[i].sequence); 
						
		 /* Write to significant only file */
			if (sig >= SIG_THRESH) {
				fprintf(fs, "%s, %d, %d, %g, %d\n",records[i].seqStr, seq1_reads, min_diffr, sig, wdiff); 
				fprintf(fs, "%s\n",records[i].sequence); 
			}
			
	 /* These are sequences with no 1-away sequences with higher counts - thus significant */
		} else {
			sig = -log10(SIG_NCOMPU);
			
			fprintf(fo, "%s, %d, %d, %g, %d\n",records[i].seqStr, seq1_reads, min_diffr, sig, wdiff); 
			fprintf(fo, "%s\n",records[i].sequence); 
			fprintf(fs, "%s, %d, %d, %g, %d\n",records[i].seqStr, seq1_reads, min_diffr, sig, wdiff); 
			fprintf(fs, "%s\n",records[i].sequence); 
		}
	}
	fclose(fs);
	fclose(fo);
		
}
		





