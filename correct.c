#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define ALIGN_OFFS		64
#define MAX_LENGTH		500

/* Locus */
#define AMP1			1
#define AMP2			2
#define AMP3			3
#define AMP4			4

int locus = AMP1;

/* Read Direction */
#define FORWARD			0
#define REVERSE			1

int direction = FORWARD;

static FILE *fp, *fo;

static int count = 0;
int next_sequence ( void )
{
	char buf[1024];
	char target[1024];
	
	target[0] = 0;
	while (fgets(buf, 512, fp)) {
		sscanf(buf, "%s", target); 
		if (!strcmp(target, "------------")) break;
		if (feof(fp)) break;
	}
	if (feof(fp)) exit(1);	/* all done */
	count++; 
	return (1);
	
}   /* Ready for next sequence */
	
	
int main (int argc, char **argv)
{
	int i, j, k, len;
	int n_seq;
	char buf[1024];
	char *ptr, *str;
	char read[MAX_LENGTH];
	char poly[MAX_LENGTH];
	char refs[MAX_LENGTH];
	char corr[MAX_LENGTH];
	
	char locusStr[64];
	int readLines;

	char inFileName[1280];
	char inFileRoot[1280];
	char outFileName[1280];
	char directionStr[64];
	
	if (argc != 2) {
		printf("\n\n");
		printf("****************************************************   \n\n");
		printf("Usage correct inFileName locus                         \n\n");
		printf("inFileName: must have'.txt' suffix and locus '_Amp(n)_'\n\n");
		printf("inFileName: must have direction '_F_' or '_R_'         \n\n");
		printf("****************************************************   \n\n");
		exit(1);
		
	} else {	/* Parse arguments */
		len = strlen (argv[1]);
		if (!strstr(argv[1], ".txt")) {
			printf("Error, inFileName does not contain '.txt' suffix\n\n");
			exit(1);
		}
		strcpy(inFileName, argv[1]);
		
		/* Check valid file */
		if ((fp = fopen (inFileName, "r")) == NULL) {
			printf("***************************************** \n\n");
			printf("Error, Unable to open input file: %s\n\n", inFileName);
			printf("***************************************** \n\n");
			exit(1);
		}
		strcpy(inFileRoot, inFileName);
		ptr = strstr(inFileRoot, ".txt");
		*ptr=0;	
		
		/* Check valid locus in file name */
		if (strstr(argv[1],        "_Amp1_")) {
			locus = AMP1;
		} else if (strstr(argv[1], "_Amp2_")) {
			locus = AMP2;
		} else if (strstr(argv[1], "_Amp3_")) {
			locus = AMP3;
		} else if (strstr(argv[1], "_Amp4_")) {
			locus = AMP4;
		} else {
			printf("***************************************** \n\n");
			printf("Error, invalid final name, no locus %s\n\n", argv[1]);
			printf("***************************************** \n\n");
			exit(1);
		}
		
		/* Check valid locus in file name */
		if (strstr(argv[1], "_F_")) {
			direction = FORWARD;
		} else if (strstr(argv[1], "_R_")) {
			direction = REVERSE;
		} else {
			printf("***************************************** \n\n");
			printf("Error, invalid final name, no direction %s\n\n", argv[1]);
			printf("***************************************** \n\n");
			exit(1);
		}
	}	
	
 /* Set locus specific variable readLines and output data read labels based on command line arguments */
	if (locus == AMP1) {
		strcpy(locusStr, "Amp1");
		readLines = 0;
	} else if (locus == AMP2) {
		strcpy(locusStr, "Amp2");
		readLines = 7;
	} else if (locus == AMP3) {
		strcpy(locusStr, "Amp3");
		readLines = 6;
	} else {
		strcpy(locusStr, "Amp4");
		readLines = 6;
	}

 /* Set direction strings for output sequences in FASTA format */
	if (direction == FORWARD) {
		strcpy(directionStr, "Forward");
	}
	if (direction == REVERSE) {
		strcpy(directionStr, "Reverse");
	}
	
 /* Output output file */
	sprintf(outFileName, "%s.fna", inFileRoot);
	if ((fo = fopen (outFileName, "w")) == NULL) {
		printf("Error: unable to open output file: %s\n", outFileName);
		exit(1);
	}
	
 /* Process the alignment and correct */
	n_seq = 0;
	while (next_sequence ()) {
		for (i=0; i<7; i++) { fgets(buf, 512, fp);}
		for (i=0; i<readLines; i++) {
			for (j=0; j<3; j++) {
				fgets(buf, 512, fp); buf[strlen(buf)-1]=0;  
				ptr = &buf[7]; ptr[ALIGN_OFFS]=0;
				if (j==0)
					strcpy(&(read[ALIGN_OFFS*i]), ptr);
				else if (j==1)
					strcpy(&(poly[ALIGN_OFFS*i]), ptr);
				else 
					strcpy(&(refs[ALIGN_OFFS*i]), ptr);				
			}
			fgets(buf, 512, fp);
		}
		for (j=0; j<3; j++) {
			fgets(buf, 512, fp); buf[strlen(buf)-1]=0;
			ptr = &buf[7];		
			if (j==0) { k=0; while (ptr[k++] != ' '); len=k-1; ptr[len]=0; }
			else {
				ptr[len]=0;
			}
			if (j==0)
				strcpy(&(read[ALIGN_OFFS*readLines]), ptr);
			else if (j==1)
				strcpy(&(poly[ALIGN_OFFS*readLines]), ptr);
			else 
				strcpy(&(refs[ALIGN_OFFS*readLines]), ptr);
		}
		fgets(buf, 512, fp); 
		
	 /* Now lets make a new sequence without indels with respect to refs */
		len = strlen (read); k=0; 
		for (i=0; i< len; i++) {
			if (poly[i] == '|') {			/* Match */
				corr[k++] = read[i];
				continue;
			}
		 /* Substitution */
			if ((refs[i] != '-') && (read[i] != '-')) {  
				corr[k++] = read[i];
				continue;
			}			
		 /* Insert in Read */
			if (refs[i] == '-') {	
				/* NO OP - ignore, do not include in the corrected sequence, don't increase length */
				continue;
			}			
		 /* Delete in read */
			if (read[i] == '-') {					
				corr[k++] = refs[i];
				continue;
			}
		}
		corr[k] = 0; n_seq++;
		
	 /* Print out corrected sequence */
		fprintf(fo, "> %s %s Indel Corrected, Read %d\n", locusStr, directionStr, n_seq);
		for (j=0; j<strlen(corr); j++) {
			fprintf(fo, "%c", corr[j]);
			if (((j+1) % 70) == 0) fprintf(fo, "\n");
		}
		fprintf(fo, "\n");

	}
	fclose(fp);
	fclose(fo);
}
		
