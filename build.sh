#!/bin/ksh

#Build capsidProcess
echo cc capsidProcess.c -lm -o capsidProcess
cc capsidProcess.c -lm -o capsidProcess

#Build filtering program - extract loci based on primer sequences
echo cc filter.c -lm -o filter
cc filter.c -lm -o filter

#Build correct program - correcrd indels to MNV-1 reference genome
echo cc correct.c -lm -o correct
cc correct.c -lm -o correct

#Build match program - cluster corrected reads
echo cc match.c -lm -o match
cc match.c -lm -o match

#Build compare program - compare forward and reverse reads
echo cc compare.c -lm -o compare
cc compare.c -lm -o compare

#Build Random Solution Generator
echo cc sig.c kfunc.c -lm -o sig
cc sig.c kfunc.c -lm -o sig
