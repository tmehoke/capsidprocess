#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int main ( int argc, char *argv[] )
{
	FILE *fp;
	char command[1024];
	char fileName1[2560], fileName2[2560];
	char fileName3[2560];
	char READS_FILE[2560];
	
	int doFilterFlag = 1;
	int doAlignmFlag = 1;
	int doCorrecFlag = 1;
	int doMatcheFlag = 1;
	int doComparFlag = 1;
	int doSignifFlag = 1;

	if (argc != 2) {
		printf("\n\n");
		printf("************************************************************ \n\n");
		printf("Usage capsidProcess  inReadFileRoot (no '.fna' suffix)       \n\n");
		printf("************************************************************ \n\n");
		exit(1);
		
	} else {	/* Parse arguments */
		strcpy(READS_FILE, argv[1]);
		sprintf(fileName1, "%s.fna", READS_FILE);
		if ((fp=fopen (fileName1, "r")) == NULL) {
			printf("Unable to open input file %s for reading, exiting...\n", fileName1);
			exit(1);
		}
		fclose(fp);
	}
		
	/* Parse input arguments and flags for run */
	
	/* Filter run file for valid reads and write out files based on primer selection */
	if (doFilterFlag) {
			
		/* Execute */
		sprintf(command, "./filter %s.fna %s %s\n", READS_FILE, "AMP2", "FORWARD");
		printf("%s", command);
		system (command);

		sprintf(command, "./filter %s.fna %s %s\n", READS_FILE, "AMP2", "REVERSE");
		printf("%s", command);
		system (command);

		sprintf(command, "./filter %s.fna %s %s\n", READS_FILE, "AMP3", "FORWARD");
		printf("%s", command);
		system (command);
		
		sprintf(command, "./filter %s.fna %s %s\n", READS_FILE, "AMP3", "REVERSE");
		printf("%s", command);
		system (command);
		
		sprintf(command, "./filter %s.fna %s %s\n", READS_FILE, "AMP4", "FORWARD");
		printf("%s", command);
		system (command);
		
		sprintf(command, "./filter %s.fna %s %s\n", READS_FILE, "AMP4", "REVERSE");
		printf("%s", command);
		system (command);

	}
	
	/* Do alignments to Cambrdige Reference Sequence */
	if (doAlignmFlag) {
				
		/* Execute */
		sprintf(fileName1, "%s_1_Amp2_F.fna", READS_FILE);
		sprintf(fileName2, "%s_2_Amp2_F_AlignMNV.txt", READS_FILE);
		if ((fp = fopen(fileName2, "r")) != NULL) {
			fclose(fp);
			sprintf(command, "rm %s\n", fileName2);
			printf("%s", command);
			system(command);
		}
		if ((fp = fopen (fileName2, "r"))) {
			sprintf(command, "rm %s", fileName2);
			system (command);
		}
		sprintf(command, "exonerate -m affine:local -s 2200 %s capsid_amplicon2_F.fna >> %s\n",
				fileName1, fileName2);		
		printf("%s", command);
		system (command);

		sprintf(fileName1, "%s_1_Amp2_R.fna", READS_FILE);
		sprintf(fileName2, "%s_2_Amp2_R_AlignMNV.txt", READS_FILE);
		if ((fp = fopen(fileName2, "r")) != NULL) {
			fclose(fp);
			sprintf(command, "rm %s\n", fileName2);
			printf("%s", command);
			system(command);
		}
		if ((fp = fopen (fileName2, "r"))) {
			sprintf(command, "rm %s", fileName2);
			system (command);
		}
		sprintf(command, "exonerate -m affine:local -s 2200 %s capsid_amplicon2_R.fna >> %s\n",
				fileName1, fileName2);		
		printf("%s", command);
		system (command);
		
		sprintf(fileName1, "%s_1_Amp3_F.fna", READS_FILE);
		sprintf(fileName2, "%s_2_Amp3_F_AlignMNV.txt", READS_FILE);
		if ((fp = fopen(fileName2, "r")) != NULL) {
			fclose(fp);
			sprintf(command, "rm %s\n", fileName2);
			printf("%s", command);
			system(command);
		}
		if ((fp = fopen (fileName2, "r"))) {
			sprintf(command, "rm %s", fileName2);
			system (command);
		}
		sprintf(command, "exonerate -m affine:local -s 1900 %s capsid_amplicon3_F.fna >> %s\n",
				fileName1, fileName2);		
		printf("%s", command);
		system (command);
		
		sprintf(fileName1, "%s_1_Amp3_R.fna", READS_FILE);
		sprintf(fileName2, "%s_2_Amp3_R_AlignMNV.txt", READS_FILE);
		if ((fp = fopen(fileName2, "r")) != NULL) {
			fclose(fp);
			sprintf(command, "rm %s\n", fileName2);
			printf("%s", command);
			system(command);
		}
		if ((fp = fopen (fileName2, "r"))) {
			sprintf(command, "rm %s", fileName2);
			system (command);
		}
		sprintf(command, "exonerate -m affine:local -s 1900 %s capsid_amplicon3_R.fna >> %s\n",
				fileName1, fileName2);		
		printf("%s", command);
		system (command);

		sprintf(fileName1, "%s_1_Amp4_F.fna", READS_FILE);
		sprintf(fileName2, "%s_2_Amp4_F_AlignMNV.txt", READS_FILE);
		if ((fp = fopen(fileName2, "r")) != NULL) {
			fclose(fp);
			sprintf(command, "rm %s\n", fileName2);
			printf("%s", command);
			system(command);
		}
		if ((fp = fopen (fileName2, "r"))) {
			sprintf(command, "rm %s", fileName2);
			system (command);
		}
		sprintf(command, "exonerate -m affine:local -s 1900 %s capsid_amplicon4_F.fna >> %s\n",
				fileName1, fileName2);		
		printf("%s", command);
		system (command);
		
		sprintf(fileName1, "%s_1_Amp4_R.fna", READS_FILE);
		sprintf(fileName2, "%s_2_Amp4_R_AlignMNV.txt", READS_FILE);
		if ((fp = fopen(fileName2, "r")) != NULL) {
			fclose(fp);
			sprintf(command, "rm %s\n", fileName2);
			printf("%s", command);
			system(command);
		}
		if ((fp = fopen (fileName2, "r"))) {
			sprintf(command, "rm %s", fileName2);
			system (command);
		}
		sprintf(command, "exonerate -m affine:local -s 1900 %s capsid_amplicon4_R.fna >> %s\n",
				fileName1, fileName2);		
		printf("%s", command);
		system (command);

	}	
	
	/* Correct Aligned reads to rCRS reference to remove indels from reads */
	if (doCorrecFlag) {
		

		/* Execute */
		sprintf(fileName1, "%s_2_Amp2_F_AlignMNV.txt", READS_FILE);		
		sprintf(command, "./correct %s\n", fileName1);
		printf("%s", command);
		system (command);
		
		sprintf(fileName1, "%s_2_Amp2_R_AlignMNV.txt", READS_FILE);		
		sprintf(command, "./correct %s\n", fileName1);
		printf("%s", command);
		system (command);
	
		sprintf(fileName1, "%s_2_Amp3_F_AlignMNV.txt", READS_FILE);		
		sprintf(command, "./correct %s\n", fileName1);
		printf("%s", command);
		system (command);
		
		sprintf(fileName1, "%s_2_Amp3_R_AlignMNV.txt", READS_FILE);		
		sprintf(command, "./correct %s\n", fileName1);
		printf("%s", command);
		system (command);

		sprintf(fileName1, "%s_2_Amp4_F_AlignMNV.txt", READS_FILE);		
		sprintf(command, "./correct %s\n", fileName1);
		printf("%s", command);
		system (command);
		
		sprintf(fileName1, "%s_2_Amp4_R_AlignMNV.txt", READS_FILE);		
		sprintf(command, "./correct %s\n", fileName1);
		printf("%s", command);
		system (command);

	}
	
	/* Match forward and reverse reads to get valid haplotypes */
	if (doMatcheFlag) {
		

		/* Execute */
		sprintf(fileName1, "%s_2_Amp2_F_AlignMNV.fna",   READS_FILE);		
		sprintf(fileName2, "%s_4_Amp2_F_AlignMNV_Unique.txt", READS_FILE);		
		sprintf(command, "./match %s %s\n", fileName1, fileName2);
		printf("%s", command);
		system (command);
		
		sprintf(fileName1, "%s_2_Amp2_R_AlignMNV.fna",   READS_FILE);		
		sprintf(fileName2, "%s_4_Amp2_R_AlignMNV_Unique.txt", READS_FILE);		
		sprintf(command, "./match %s %s\n", fileName1, fileName2);
		printf("%s", command);
		system (command);
		
		sprintf(fileName1, "%s_2_Amp3_F_AlignMNV.fna",   READS_FILE);		
		sprintf(fileName2, "%s_4_Amp3_F_AlignMNV_Unique.txt", READS_FILE);		
		sprintf(command, "./match %s %s\n", fileName1, fileName2);
		printf("%s", command);
		system (command);
		
		sprintf(fileName1, "%s_2_Amp3_R_AlignMNV.fna",   READS_FILE);		
		sprintf(fileName2, "%s_4_Amp3_R_AlignMNV_Unique.txt", READS_FILE);		
		sprintf(command, "./match %s %s\n", fileName1, fileName2);
		printf("%s", command);
		system (command);

		sprintf(fileName1, "%s_2_Amp4_F_AlignMNV.fna",   READS_FILE);		
		sprintf(fileName2, "%s_4_Amp4_F_AlignMNV_Unique.txt", READS_FILE);		
		sprintf(command, "./match %s %s\n", fileName1, fileName2);
		printf("%s", command);
		system (command);
		
		sprintf(fileName1, "%s_2_Amp4_R_AlignMNV.fna",   READS_FILE);		
		sprintf(fileName2, "%s_4_Amp4_R_AlignMNV_Unique.txt", READS_FILE);		
		sprintf(command, "./match %s %s\n", fileName1, fileName2);
		printf("%s", command);
		system (command);

	}

	/* Compare forward and reverse unique sequences and record corroborating sequences */
	if (doComparFlag) {
		
		/* Execute */
		sprintf(fileName1, "%s_4_Amp2_F_AlignMNV_Unique.txt", READS_FILE);		
		sprintf(fileName2, "%s_4_Amp2_R_AlignMNV_Unique.txt", READS_FILE);		
		sprintf(fileName3, "%s_5_Amp2.fna", READS_FILE);		
		sprintf(command, "./compare %s %s %s\n", fileName1, fileName2, fileName3);
		printf("%s", command);
		system (command);

		sprintf(fileName1, "%s_4_Amp3_F_AlignMNV_Unique.txt", READS_FILE);		
		sprintf(fileName2, "%s_4_Amp3_R_AlignMNV_Unique.txt", READS_FILE);		
		sprintf(fileName3, "%s_5_Amp3.fna", READS_FILE);		
		sprintf(command, "./compare %s %s %s\n", fileName1, fileName2, fileName3);
		printf("%s", command);
		system (command);
		
		sprintf(fileName1, "%s_4_Amp4_F_AlignMNV_Unique.txt", READS_FILE);		
		sprintf(fileName2, "%s_4_Amp4_R_AlignMNV_Unique.txt", READS_FILE);		
		sprintf(fileName3, "%s_5_Amp4.fna", READS_FILE);		
		sprintf(command, "./compare %s %s %s\n", fileName1, fileName2, fileName3);
		printf("%s", command);
		system (command);

	}
	
	if (doSignifFlag) {
		

		/* Execute */
		sprintf(fileName1, "%s_5_Amp2.genotypes.fna", READS_FILE);		
		sprintf(command, "./sig %s\n", fileName1);
		printf("%s", command);
		system (command);
		
		/* Execute */
		sprintf(fileName1, "%s_5_Amp3.genotypes.fna", READS_FILE);		
		sprintf(command, "./sig %s\n", fileName1);
		printf("%s", command);
		system (command);
		
		/* Execute */
		sprintf(fileName1, "%s_5_Amp4.genotypes.fna", READS_FILE);		
		sprintf(command, "./sig %s\n", fileName1);
		printf("%s", command);
		system (command);
		
	}
	
	
}
