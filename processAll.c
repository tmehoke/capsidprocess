#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main ( int argc, char *argv[] )
{
	int i;
	char buf[1024];
	char fName[256];
	char command[1024];
	FILE *fp;
	
	if ((fp = fopen ("newFiles.txt", "r")) == NULL) {
		fprintf(stderr, "Cannot open file list... exiting\n");
		exit(1);
	}
	while (fgets(buf, 1024, fp)) {
		sscanf(buf, "%s", fName);
		sprintf(command, "./capsidProcess data/%s", fName);
		system (command);
	}
}