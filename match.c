#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

char **read454;

#define MAX_LENGTH 500

/* Locus */
#define AMP1			1
#define AMP2			2
#define AMP3			3
#define AMP4			4

/* Read Direction */
#define FORWARD			0
#define REVERSE			1

/* Reverse complement primer sequences */

#define AMP2_FORWARD_PRIMER_RC		"GAAGCATG"
#define AMP2_REVERSE_PRIMER_RC		"TGGGTGTC"

#if 0
#define AMP3_FORWARD_PRIMER_RC		"AGGACACC"
#define AMP3_REVERSE_PRIMER_RC		"GGTCCCTT"
#endif

#define AMP3_FORWARD_PRIMER_RC		"AGGACACC"
#define AMP3_REVERSE_PRIMER_RC		"GGTTCCC"


#define AMP4_FORWARD_PRIMER_RC		"ATGGTGTC"
#define AMP4_REVERSE_PRIMER_RC		"ATGCTCAA"

int locus = AMP2;
int direc = FORWARD;

#define PRIMER_LENGTH	15

int main (int argc, char **argv)
{
	int i, j, k;
	char buf[1024], str[256];
	char fName[2560];

	int mReads;
	int *visiReads;
	int *goodReads;

	int rIndex, len;
	int nMatchedReads;
	int matchCount;
	unsigned long gReads;
	
	int threshold;
	
	FILE *fp, *fo;
	char iFileName[1280];
	char oFileName[1280];

	char *ptr;
	char primerStr[24];
	char tSequence[1024];
	int missed=0;
	
	int totalMatches=0;
	int lenCpy;
	
	if (argc != 3) {
		printf("\n\n");
		printf("***************************************** \n\n");
		printf("Usage correct inFileName oFileName        \n\n");
		printf("***************************************** \n\n");
		exit(1);
		
	} else {	/* Parse arguments */
		
		/* Check valid locus in file name */
		if (strstr(argv[1],        "_Amp1_")) {
			locus = AMP1;
		} else if (strstr(argv[1], "_Amp2_")) {
			locus = AMP2;
		} else if (strstr(argv[1], "_Amp3_")) {
			locus = AMP3;
		} else if (strstr(argv[1], "_Amp4_")) {
			locus = AMP4;
		} else {
			printf("***************************************** \n\n");
			printf("Error, invalid final name, no locus %s\n\n", argv[1]);
			printf("***************************************** \n\n");
			exit(1);
		}
		
		/* Check valid locus in file name */
		if (strstr(argv[1], "_F_")) {
			direc = FORWARD;
		} else if (strstr(argv[1], "_R_")) {
			direc = REVERSE;
		} else {
			printf("***************************************** \n\n");
			printf("Error, invalid final name, no direction %s\n\n", argv[1]);
			printf("***************************************** \n\n");
			exit(1);
		}
		
		/* Check valid file */
		strcpy(iFileName, argv[1]);
		if ((fp = fopen (iFileName, "r")) == NULL) {
			printf("***************************************** \n\n");
			printf("Error, Unable to open input file: %s\n\n", iFileName);
			printf("***************************************** \n\n");
			exit(1);
		}
		
		/* Check valid file */
		strcpy(oFileName, argv[2]);
		if ((fo = fopen (oFileName, "w")) == NULL) {
			printf("***************************************** \n\n");
			printf("Error, Unable to open output file: %s\n\n", oFileName);
			printf("***************************************** \n\n");
			exit(1);
		}
	}	
	 		
 /* Get number of reads from raw 454 data */
	gReads = 0L;
	while (fgets(buf, 1000, fp)) {
		if (buf[0] == '>') gReads++;
 	}
	rewind(fp);
	
 /* Allocate for 454 read data */
	read454 = (char **) calloc (gReads, sizeof(char *));
	for (i=0; i<gReads; i++) {
		read454[i] = (char *) calloc(MAX_LENGTH, sizeof(char));
	}
	visiReads = (int *) calloc(gReads, sizeof(int));
	
 /* Read in the FASTA format data */
    mReads =  0;
    rIndex =  0;
	fgets(buf, 1000, fp);					/* Ignore first '>' read marker */
	while (fgets(buf, 1000, fp)) {
		buf[strlen(buf)-1] = 0;				/* Get rid of CR/LF			*/
		if (buf[0] == '>') {				/* End of previous read or new read */
			rIndex = 0;						/* Reset sequencing index 	*/
			mReads++;
		} else {
			len = strlen(buf); 
			for (i=0; i<len; i++) read454[mReads][rIndex++] = buf[i];		/* Strings should self terminate as calloc pads with zeros */
		}
 	}
	fclose(fp);
	
	/* Perform trimming off of primers to make lengths uniform */
	if (locus == AMP2 && direc == FORWARD) {
		strcpy(primerStr, AMP2_REVERSE_PRIMER_RC);
		
	} else if (locus == AMP2 && direc == REVERSE) {
		strcpy(primerStr, AMP2_FORWARD_PRIMER_RC);
		
	} else if (locus == AMP3 && direc == FORWARD) {
		strcpy(primerStr, AMP3_REVERSE_PRIMER_RC);
		
	} else if (locus == AMP3 && direc == REVERSE) {
		strcpy(primerStr, AMP3_FORWARD_PRIMER_RC);
		
	} else if (locus == AMP4 && direc == FORWARD) {
		strcpy(primerStr, AMP4_REVERSE_PRIMER_RC);
		
	} else {
		strcpy(primerStr, AMP4_FORWARD_PRIMER_RC);
	}
	
	for (i=0; i< gReads; i++) {
		strcpy (tSequence, read454[i]);
		
		/* Now condition sequence by removing primers */
		if ((ptr = strstr(tSequence, primerStr)) == NULL) {
			missed++;
			/* Complementary primaer not found at end of sequence - ignore these reads */
			/* This can be a problem if something fixes in the primer site, but at the few percent level - OK */
			visiReads[i]=1;		/* Will ignore these below */
			continue;
		}
		*ptr=0;	/* Lop off the opposite primer on end of read */

		if (locus == AMP3 && direc == REVERSE) {
			strcpy(read454[i], tSequence+PRIMER_LENGTH + 22);	/* Lop off primer at the beginning + homopoly area */
		} else {
			strcpy(read454[i], tSequence+PRIMER_LENGTH);		/* Lop off primer at the beginning */
		}	
	}	

 /* Threshold  total reads */
	threshold = 1;			// Get all the reads for now
	totalMatches = 0;
	
 /* Find and output all unique reads */
	nMatchedReads = 0;
	for (i=0; i< gReads; i++) {		
		if (visiReads[i]) continue;		/* Don't check visited reads */
		
		visiReads[i] = 1;				/* Visit this read */
		matchCount = 1;					/* We will print out anything in the DB, even if only 1 copy */
		for (j=0; j<gReads; j++) {
			if (visiReads[j]) continue;	/* Don't check visited reads */
			
		 /* Accumulate counts of reads match read i */
			if (!strcmp(read454[i], read454[j])) {
				matchCount++;
				visiReads[j] = 1;
			}
		}
		
	 /* Ignore any reads that are below threshold */
		if (matchCount >= threshold) {  
			fprintf(fo, "%4d\t%4d\t%f\t%s\n", i, matchCount, 1.*matchCount/gReads, read454[i]);	
			nMatchedReads++;
			totalMatches += matchCount;
		}
	}
	fclose(fo);
	
	printf("Number of matched reads = %d\n", nMatchedReads);
	printf("Number of ignored reads = %d\n", missed);
	printf("Total matched reads all = %d\n", totalMatches);
}
	
