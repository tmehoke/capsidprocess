#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAX_LENGTH		1000

int main (int argc, char **argv)
{
	unsigned int i, j, k;
	char buf[1024], str[256];
	
	int fReads, rReads;
	char **fSequence, **rSequence;
	int index, *fCount, *rCount;
	float *fFract, *rFract;
	char tSequence[1024];
	char *tPtr;
	char base, rBase;
	int len;
	int matchLen;
	int total = 0;
	int seqTotal;
	char *ptr;
	
	char fFileName[1280];
	char rFileName[1280];
	char oFileName[1280];
	char cFileName[1280];
	
	char locusStr[64];

	FILE *ff=NULL;
	FILE *fr=NULL;
	FILE *fo=NULL;
	FILE *fc=NULL;
	
	float ratio;
	
	sprintf(fFileName, "%s_seqs.txt", argv[1]);
	FILE *fa = fopen (fFileName, "w");
	
	if (argc != 4) {
		printf("\n\n");
		printf("****************************************************** \n\n");
		printf("Usage correct forwardFileName reversFileName oFileName \n\n");
		printf("****************************************************** \n\n");
		exit(1);
		
	} else {	/* Parse arguments */
				
		/* Check valid locus in file name */
		if (strstr(argv[1],        "_Amp1_")) {
			strcpy(locusStr, "Amp1");
		} else if (strstr(argv[1], "_Amp2_")) {
			strcpy(locusStr, "Amp2");
		} else if (strstr(argv[1], "_Amp3_")) {
			strcpy(locusStr, "Amp3");
		} else if (strstr(argv[1], "_Amp4_")) {
			strcpy(locusStr, "Amp4");
		} else {
			printf("***************************************** \n\n");
			printf("Error, invalid final name, no locus %s\n\n", argv[1]);
			printf("***************************************** \n\n");
			exit(1);
		}

		/* Check valid file */
		strcpy(fFileName, argv[1]);
		if ((ff = fopen (fFileName, "r")) == NULL) {
			printf("***************************************** \n\n");
			printf("Error, Unable to open input file: %s\n\n", fFileName);
			printf("***************************************** \n\n");
			exit(1);
		}

		/* Check valid file */
		strcpy(rFileName, argv[2]);
		if ((fr = fopen (rFileName, "r")) == NULL) {
			printf("***************************************** \n\n");
			printf("Error, Unable to open input file: %s\n\n", rFileName);
			printf("***************************************** \n\n");
			exit(1);
		}
		
		/* Check valid file */
		strcpy(oFileName, argv[3]);
		if ((fo = fopen (oFileName, "w")) == NULL) {
			printf("***************************************** \n\n");
			printf("Error, Unable to open output file: %s\n\n", oFileName);
			printf("***************************************** \n\n");
			exit(1);
		}
		ptr = strstr(oFileName, ".fna");
		if (!ptr) {
			printf("Output file must end in .fna, exiting...\n");
			exit(1);
		}
		*ptr=0;
		sprintf(cFileName, "%s.genotypes.fna", oFileName);
		if ((fc = fopen (cFileName, "w")) == NULL) {
			printf("***************************************** \n\n");
			printf("Error, Unable to open output file: %s\n\n", oFileName);
			printf("***************************************** \n\n");
			exit(1);
		}
	}	
	
 /* Get unique sequences in forward */
	fReads = 0L;
	while (fgets(buf, 1000, ff)) fReads++;
	rewind(ff);

	fFract = (float *) calloc (fReads, sizeof(float));
	fCount = (int *) calloc(fReads, sizeof(int));
	fSequence = (char **) calloc (fReads, sizeof(char *));
	for (i=0; i<fReads; i++) {
		fSequence[i] = (char *) calloc(MAX_LENGTH, sizeof(char));
	}	
	i=0;
 	while (fgets(buf, 1000, ff)) {
		sscanf(buf,"%d %d %f %s", &index, &(fCount[i]), &(fFract[i]), fSequence[i]);
		i++;
 	}
	fclose(ff);
	printf("Number of forward sequences parsed: %d\n", fReads);

 /* Get unique sequences in reverse */
	rReads = 0L;
	while (fgets(buf, 1000, fr)) rReads++;
	rewind(fr);
	
	rFract = (float *) calloc (rReads, sizeof(float));
	rCount = (int *) calloc(rReads, sizeof(int));
	rSequence = (char **) calloc (rReads, sizeof(char *));
	for (i=0; i<rReads; i++) {
		rSequence[i] = (char *) calloc(MAX_LENGTH, sizeof(char));
	}	
	i=0;
 	while (fgets(buf, 1000, fr)) {
		sscanf(buf,"%d %d %f %s", &index, &(rCount[i]), &(rFract[i]), tSequence); 
		tPtr = tSequence;
		len = strlen (tPtr);
		for (j=0; j<len; j++) {
			base = tPtr[len-j-1];
			switch (base) {
				case 'A': rBase = 'T'; break;
				case 'T': rBase = 'A'; break;
				case 'C': rBase = 'G'; break;
				case 'G': rBase = 'C'; break;
			}
			rSequence[i][j] = rBase;
		}
		i++;
 	}
	fclose(fr);
	printf("Number of reverse sequences parsed: %d\n", rReads);

 /* Count total number of reads for matching sequences */
	seqTotal = 0;
	for (i=0; i<fReads; i++) {
		matchLen = strlen(fSequence[i]);
		for (j=0; j<rReads; j++) {
			if (!strncmp(fSequence[i], rSequence[j], matchLen)) {
				seqTotal += (fCount[i] + rCount[j]);
			}
		}
	}
	
 /* Now look for all forwards in the reverse */
	for (i=0; i<fReads; i++) {
		matchLen = strlen(fSequence[i]);
		for (j=0; j<rReads; j++) {
			if (!strcmp(fSequence[i], rSequence[j])) {
				
			 /* Apply a cut here on forward reverse ratio */
				ratio = fFract[i]/rFract[j];
				if ((ratio < 0.4) || (ratio > 2.5)) continue;
				
				total++;
				fprintf(fo, ">%s_%d_%d_%d mfraction=%f, fFraction=%f rFraction=%f\n", 
						locusStr, total, fCount[i], rCount[j], 1.*(fCount[i]+rCount[j])/seqTotal, fFract[i], rFract[j]);
				fprintf(fc, ">%s_%d_%d_%d mfraction=%f, fFraction=%f rFraction=%f\n", 
						locusStr, total, fCount[i], rCount[j], 1.*(fCount[i]+rCount[j])/seqTotal, fFract[i], rFract[j]);
				fprintf(fc, "%s\n", fSequence[i]); 
					
				/* Write out filtered data for next step in pipeline */
				for (k=0; k<matchLen; k++) {
					fprintf(fo, "%c", fSequence[i][k]);
					if (((k+1) % 70) == 0) fprintf(fo, "\n");
				}
				fprintf(fo, "\n");
				fprintf(fa, "%s\n", fSequence[i]);

			}
		}
	}
	fclose(fo);	
	fclose(fc);
	printf("total forward/reverse matched sequences = %d\n", total);
	
	fclose(fa);
	
}
	



