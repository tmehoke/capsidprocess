#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

char **read454;
int *goodReads, mReads;

#define MAX_LENGTH		600

/* Locus */
#define AMP1			1
#define AMP2			2
#define AMP3			3
#define AMP4			4

int locus = AMP2;

/* Read Directions */
#define FORWARD			0
#define REVERSE			1

int direction = FORWARD;

/* Primer sequence */

#define AMP1_FORWARD_PRIMER		""
#define AMP1_REVERSE_PRIMER		""

#define AMP2_FORWARD_PRIMER		"CAGATCACATGCTTC"
#define AMP2_REVERSE_PRIMER		"AAAGCAGGACACCCA"

#define AMP3_FORWARD_PRIMER		"GTTCATGGGTGTCCT"
#define AMP3_REVERSE_PRIMER		"GGGGAGAAAGGGACC"

#define AMP4_FORWARD_PRIMER		"TTTTCAGGACACCAT"
#define AMP4_REVERSE_PRIMER		"TTATTGTTTGAGCAT"

char primer[] = AMP2_FORWARD_PRIMER;

/* READ LENGTH DISTRIBUTION CUTS READS		*/

#define AMP1_LEN_MIN	0
#define AMP1_LEN_MAX	0

#define AMP2_LEN_MIN	445
#define AMP2_LEN_MAX	455

#if 0
#define AMP3_LEN_MIN	410
#define AMP3_LEN_MAX	435
#endif

#define AMP3_LEN_MIN	410
#define AMP3_LEN_MAX	450

#define AMP4_LEN_MIN	410
#define AMP4_LEN_MAX	435

/* THE ABOVE SHOULD NOT CHANGE ONCE PRIMERS ARE FIXED	*/


/* Prototype */
int filter_reads ( void );

int main (int argc, char **argv)
{
	FILE *fp=NULL;
	FILE *fo=NULL;
	char fName[2560];
	
	unsigned long i, j;
	char buf[1024], str[256];
	char temp1[512], temp2[512], temp[512];

	int rIndex, len, rCount;
	int fPassed;
	unsigned long gReads;
	
	char *ptr, *sptr;
	
	char locusStr[64];
	char primerStr[64];
	char directionStr[64];
	char outFileName[1280];
	char inFileName[1280];
	char inFileRoot[1280];
		
 /* Parse commandline arguments */
	if (argc != 4) {
		printf("\n\n");
		printf("***************************************************** \n\n");
		printf("Usage filter inFileName locus direction               \n\n");
		printf("inFileName:        must include '.fna'                \n\n");
		printf("locus values:     'AMP1' or 'AMP2' or 'AMP3' or 'AMP4'\n\n");
		printf("direction values: 'FORWARD' or 'REVERSE'              \n\n");
		printf("***************************************************** \n\n");
		exit(1);
	
	} else {	/* Parse arguments */
		len = strlen (argv[1]);
		if (!strstr(argv[1], ".fna")) {
			printf("Error, inFileName does not contain '.fna' suffix\n\n");
			exit(1);
		}
		strcpy(inFileName, argv[1]);
		
	 /* Check valid file */
		if ((fp = fopen (inFileName, "r")) == NULL) {
			printf("***************************************** \n\n");
			printf("Error, Unable to open input file: %s\n\n", inFileName);
			printf("***************************************** \n\n");
			exit(1);
		}
		strcpy(inFileRoot, inFileName);
		ptr = strstr(inFileRoot, ".fna");
		*ptr=0;			
		
	 /* Check valid locus */
		if (!strcmp(argv[2], "AMP1")) {
			locus = AMP1; printf("AMP1 not implemented...\n"); exit(1);
		} else if (!strcmp(argv[2], "AMP2")) {
			locus = AMP2;
		} else if (!strcmp(argv[2], "AMP3")) {
			locus = AMP3;
		} else if (!strcmp(argv[2], "AMP4")) {
			locus = AMP4;
		} else {
			printf("***************************************** \n\n");
			printf("Error, invalid locus value: %s\n\n", argv[2]);
			printf("***************************************** \n\n");
			exit(1);
		}

	 /* Check valid locus */
		if (!strcmp(argv[3], "FORWARD")) {
			direction = FORWARD;
		} else if (!strcmp(argv[3], "REVERSE")) {
			direction = REVERSE;
		} else {
			printf("***************************************** \n\n");
			printf("Error, invalid read direction value: %s\n\n", argv[3]);
			printf("***************************************** \n\n");
			exit(1);
		}
	}
	
 /* Set read selection and output data read labels based on command line arguments */
	if (locus == AMP1) strcpy(locusStr, "Amp1");
	if (locus == AMP2) strcpy(locusStr, "Amp2");
	if (locus == AMP3) strcpy(locusStr, "Amp3");
	if (locus == AMP4) strcpy(locusStr, "Amp4");
	
	if (direction == FORWARD) strcpy(directionStr, "F");
	if (direction == REVERSE) strcpy(directionStr, "R");
	
	if ((locus == AMP1) && (direction == FORWARD)) strcpy(primerStr, AMP1_FORWARD_PRIMER);
	if ((locus == AMP1) && (direction == REVERSE)) strcpy(primerStr, AMP1_REVERSE_PRIMER);
	if ((locus == AMP2) && (direction == FORWARD)) strcpy(primerStr, AMP2_FORWARD_PRIMER);
	if ((locus == AMP2) && (direction == REVERSE)) strcpy(primerStr, AMP2_REVERSE_PRIMER);	
	if ((locus == AMP3) && (direction == FORWARD)) strcpy(primerStr, AMP3_FORWARD_PRIMER);
	if ((locus == AMP3) && (direction == REVERSE)) strcpy(primerStr, AMP3_REVERSE_PRIMER);
	if ((locus == AMP4) && (direction == FORWARD)) strcpy(primerStr, AMP4_FORWARD_PRIMER);
	if ((locus == AMP4) && (direction == REVERSE)) strcpy(primerStr, AMP4_REVERSE_PRIMER);	
	
 /* Get number of reads from raw 454 data */
	gReads = 0L;
	while (fgets(buf, 1000, fp)) {
		if (buf[0] == '>') gReads++;
 	}
	rewind(fp);
	
 /* Allocate for 454 read data */
	read454 = (char **) calloc (gReads, sizeof(char *));
	for (i=0; i<gReads; i++) {
		read454[i] = (char *) calloc(MAX_LENGTH, sizeof(char));
	}
	goodReads = (int *) calloc(gReads, sizeof(int));
	
 /* Read in the FASTA format data */
    mReads =  0;
    rIndex =  0;
	fgets(buf, 1000, fp);					/* Ignore first '>' read marker */
	while (fgets(buf, 1000, fp)) {
		buf[strlen(buf)-1] = 0;				/* Get rid of CR/LF			*/
		if (buf[0] == '>') {				/* End of previous read or new read */
			rIndex = 0;						/* Reset sequencing index 	*/
			mReads++;
		} else {
			len = strlen(buf); 
			for (i=0; i<len; i++) read454[mReads][rIndex++] = buf[i];	/* terminated string: calloc pads with zeros */
		}
 	}
	fclose(fp);
	
 /* Filter the reads for lengths and content errors */
	filter_reads ();
	
 /* Output output file */
	sprintf(outFileName, "%s_1_%s_%s.fna", inFileRoot, locusStr, directionStr);
	if ((fp = fopen (outFileName, "w")) == NULL) {
		printf("Error: unable to open output file: %s\n", outFileName);
		exit(1);
	}

 /* Filter by primers */
	rCount  = 0;
	for (i=0; i<mReads; i++) {
		if (!(goodReads[i])) continue;
		
	 /* Check for primer site match */
		if (!strncmp(read454[i], primerStr, 12)) {		
			rCount++;
			len = strlen(read454[i]);
			fprintf(fp, "> %s %s, Read %d, Length = %d\n", locusStr, directionStr, rCount, len);
				
		 /* Write out filtered data for next step in pipeline */
			len = strlen (read454[i]);			
			for (j=0; j<len; j++) {
				fprintf(fp, "%c", read454[i][j]);
				if (((j+1) % 70) == 0) fprintf(fp, "\n");
			}
			fprintf(fp, "\n");
		}
	}
	fclose(fp);	
	printf("Number of reads processed: %d\n", rCount);
}
	
/* Function to filter by length and read content */
int filter_reads ( void )
{
	FILE *fp, *fo;
	char nextChar, *ptr, fName[256];
	int i, j, len;
	int minLen, maxLen;
	int goodRead;

 /* Set length filters bounds based on locus */
	if (locus == AMP1) {
		minLen = AMP1_LEN_MIN;
		maxLen = AMP1_LEN_MAX;
		
	} else if (locus == AMP2) {
		minLen = AMP2_LEN_MIN;
		maxLen = AMP2_LEN_MAX;
		
	} else if (locus == AMP3) {
		minLen = AMP3_LEN_MIN;
		maxLen = AMP3_LEN_MAX;
		
	} else {
		minLen = AMP4_LEN_MIN;
		maxLen = AMP4_LEN_MAX;
	}
		
 /* Filter out reads with extraneous characters */
	for (i=0; i< mReads; i++) {	
		goodReads[i] = 0;
		
	 /* check for bad reads, mostly containing "N" */
		goodRead = 1;
		len = strlen (read454[i]); 
		for (j=0; j<len; j++) {
			nextChar = read454[i][j];
			if ((nextChar != 'A') && (nextChar != 'T') && (nextChar != 'C') && (nextChar != 'G')) {
				goodRead = 0;
			}
		}
		if (!goodRead) continue;
		
		/* Put a cut on the read length  - HV2 */
		if (len < minLen || len > maxLen) continue;
		
	 /* If we make it here, the read is OK */
		goodReads[i] = 1;
	}
}
	




