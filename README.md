capsidProcess pipeline

by Andy Feldman

July 15, 2013


**Notes:**

So many aspects of this code are hard-coded that I stopped trying to fix them all.

Also, all of the binaries that this code uses need to be called from the directory
that they are located in (i.e. this directory).

Finally, this code produces a large number of temporary files that are stored in
the directory of the input file.  I have numbered these files to make it easier
to identify the order that they are produced in.


**Prerequisites:**

Exonerate must be in your $PATH variable

(it can be downloaded here: (http://www.ebi.ac.uk/~guy/exonerate/)

**Compiling instructions:**

First compile the C code:

		sh ./build.sh

To run using the test data:

		./capsidProcess ./test_data/pcrsim-0.05pct/pcr-10k

(the file extension must be .fna, but can't be included in the function call)

